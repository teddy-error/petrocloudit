export default [
  '$locationProvider',
  '$stateProvider',
  (locationProvider, stateProvider) => {
    const AppState = {
      component: 'app',
      name: 'app',
      url: '/',
    };

    const PostState = {
      component: 'post',
      name: 'post',
      resolve: {
        data (postService, $transition$) {
          'ngInject';

          return postService.get($transition$.params().postId);
        },
        comments (commentService, $transition$) {
          'ngInject';

          return commentService.get($transition$.params().postId);
        },
      },
      url: '/post/{postId}',
    };

    const UserState = {
      component: 'user',
      name: 'user',
      resolve: {
        data (userService, $transition$) {
          'ngInject';

          return userService.get($transition$.params().userId);
        },
        posts (postService, $transition$) {
          'ngInject';

          return postService.getBy($transition$.params().userId);
        },
      },
      url: '/user/{userId}',
    };

    // #! isn't needed with HTML5
    locationProvider.html5Mode(true);

    stateProvider.state(AppState);
    stateProvider.state(PostState);
    stateProvider.state(UserState);
  }];
