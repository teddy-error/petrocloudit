import 'babel-polyfill';
import angular from 'angular';
import ngResource from 'angular-resource';
import ngAnimate from 'angular-animate';
import Router from '@uirouter/angularjs';
import Config from './app.config';
import AppCore from './core';
import '../style/app.css';

const app = {
  controller:
      class AppCtrl {
        constructor ($scope) {
          'ngInject';

          $scope.siteName = 'Petrocloudit';
        }
      },
  controllerAs: 'app',
  template: require('./app.html'),
};

angular.module('app', [
  Router,
  ngAnimate,
  ngResource,
  AppCore,
]).component('app', app);

angular.module('app').config(Config);
