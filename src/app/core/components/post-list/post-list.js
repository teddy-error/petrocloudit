import './post-list.css';

// Component
export default {
  name: 'postList',
  controller: class PostListCtrl {
    constructor (postService, userService) {
      'ngInject';

      this.$onInit = () => {
        if (!this.posts) {
          this.posts = postService;
          this.posts.fetch();
          this.voted = {};
          this.users = userService;
        }
      };
    }

    /* This state is not retained between routing
    ** in a real scenario voting might be part of
    ** a user state service or integrated into the
    ** post service as an optional property
    ** 1 = Up'd; 0 = No Vote; -1 = Downed
    */
    upvote = (id) => {
      switch (this.voted[id]) {
      case 1:
        this.voted[id] = 0;
        this.posts.downvote(id);
        break;
      case undefined:
      case 0:
      case -1:
        this.voted[id] = 1;
        this.posts.upvote(id);
        break;
      }
    }

    downvote = (id) => {
      switch (this.voted[id]) {
      case 1:
      case 0:
      case undefined:
        this.voted[id] = -1;
        this.posts.downvote(id);
        break;
      case -1:
        this.voted[id] = 0;
        this.posts.upvote(id);
        break;
      }
    }
  },
  selector: 'postList',
  template: require('./post-list.html'),
};
