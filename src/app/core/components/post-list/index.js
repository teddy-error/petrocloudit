import angular from 'angular';
import PostListComponent from './post-list';

export default angular.module('postList', ['core.services'])
  .component('postList', PostListComponent) // TODO
  .name;
