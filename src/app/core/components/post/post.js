import './post.css';

// Component
export const PostComponent = {
  bindings: {
    data: '<',
    comments: '<',
  },
  name: 'post',
  template: require('./post.html'),
  controller: class PostCtrl {
    constructor (userService) {
      'ngInject';


      // Expose user service to template
      this.users = userService;
    }
  },
};
