import angular from 'angular';
import {PostComponent} from './post';

export default angular
  .module('post', [])
  .component(PostComponent.name, PostComponent)
  .name;
