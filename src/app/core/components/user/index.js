import angular from 'angular';
import UserComponent from './user';

export default angular
  .module('user', ['core.services'])
  .component(UserComponent.name, UserComponent)
  .name;
