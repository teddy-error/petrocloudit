export default {
  bindings: {
    data: '<',
    posts: '<',
  },
  controller: class UserController {
    constructor (userService) {
      'ngInject';

      this.editing = false;
      this.users = userService;
    }
    $onInit = () => {
      // Ensure username has been loaded
      if (this.data.$promise) {
        this.data.$promise.then((d) => {
          this.currentName = d.username;
        });
      } else {
        this.currentName = this.data.username;
      }
    }
    toggleEdit = () => {
      this.editing = !this.editing;
    }
    reset = () => {
      this.data.username = this.currentName;
      this.toggleEdit();
    }
    submit = () => {
      // Barebones Validation
      if (!this.data.username) {
        this.reset();
      } else {
        // TODO: check for change prior to request
        this.users.save(this.data.id);
        this.toggleEdit();
        this.currentName = this.data.username;
      }
    }
    keyHandler = (event) => {
      if (event.key === 'Escape') {
        this.reset();
      } else if (event.key === 'Enter') {
        this.submit();
      }
    }
  },
  name: 'user',
  template: require('./user.html'),
};
