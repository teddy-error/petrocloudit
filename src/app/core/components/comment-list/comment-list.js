export default {
  bindings: {data: '<'},
  name: 'comments',
  controller: class CommentsCtrl {
    constructor ($scope) {
      'ngInject';

      this.$onInit = () => {
        const parent = $scope.$parent.$ctrl;

        this.comments = parent.comments;
      };
    }
  },
  selector: 'comments',
  template: require('./comment-list.html'),
};
