import angular from 'angular';
import CommentsComponent from './comment-list';
import './comment-list.css';

export default angular
  .module('comments', ['core.services'])
  .component(CommentsComponent.name, CommentsComponent)
  .name;
