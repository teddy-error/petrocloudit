import angular from 'angular';
import PostComponent from './post';
import PostListComponent from './post-list';
import CommentsComponent from './comment-list';
import UserComponent from './user';

export default angular.module('app.components', [
  PostComponent,
  PostListComponent,
  CommentsComponent,
  UserComponent,
]);
