import angular from 'angular';

export default ['$resource', 'userService', (resource, userService) => {
  const URI = 'http://jsonplaceholder.typicode.com/comments/:commentId';
  const Comments = resource(URI, {}, {
    getFor: {
      isArray: true,
      method: 'GET',
      transformResponse (data) {
        return JSON.parse(data).map((comment) => {
          const userId = Math.floor(Math.random() * 10) + 1;
          const user = userService.get(userId);

          comment.username = user.username;
          comment.userId = userId;

          return comment;
        });
      },
      url: 'http://jsonplaceholder.typicode.com/comments?postId=:postId',
    },
  });
  const comments = {
    data: [], // TODO: Maybe a set would be better here
  };

  comments.fetch = async (callback) => {
    if (comments.data.length) {
      return;
    }

    comments.data = await Comments.query(callback);
  };

  comments.get = async (postId) => {
    if (!comments.data[postId]) {
      comments.data[postId] = await Comments.getFor({postId});
    }

    return comments.data[postId];
  };

  return comments;
}];

