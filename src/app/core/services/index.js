import angular from 'angular';
import PostService from './posts.srv';
import CommentService from './comment.srv';
import UserService from './user.srv';

// import LocalStorageModule from 'angular-local-storage';

export default angular
  .module('core.services', [])
  .service('postService', PostService)
  .service('userService', UserService)
  .service('commentService', CommentService);

