export default ['$resource', (resource) => {
  const URI = 'http://jsonplaceholder.typicode.com/users/:userId';
  const Users = resource(URI, {}, {
    query: {
      isArray: false,
      method: 'GET',
      transformResponse (data) {
        const users = {};

        JSON.parse(data).forEach((user) => {
          users[user.id] = user;
        });

        return users;
      },
    },
    update: {
      method: 'PUT',
    },
  });

  const users = {
    data: {},
  };

  users.fetch = async (callback) => {
    if (Object.keys(users.data).length) {
      return;
    }

    users.data = await Users.query(callback);
  };

  users.get = (userId) => {
    if (!users.data[userId]) {
      users.data[userId] = Users.get({userId});
    }

    return users.data[userId];
  };

  users.save = (userId) => {
    Users.update({userId}, users.data[userId]);
  };

  // Initial fetch
  users.fetch();

  return users;
}];

