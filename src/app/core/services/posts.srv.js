export default ['$resource', '$rootScope', (resource, rootScope) => {
  const URI = 'http://jsonplaceholder.typicode.com/posts/:postId';
  const posts = {
    data: {},
  };
  const Posts = resource(URI, {}, {
    getBy: {
      isArray: true,
      method: 'GET',
      url: 'http://jsonplaceholder.typicode.com/posts?userId=:userId',
    },
    query: {
      method: 'GET',
      transformResponse (data) {
        const newPosts = {};

        JSON.parse(data).forEach((post) => {
          post.rank = Math.floor(Math.random() * 140) + 10;
          newPosts[post.id] = post;
        });

        return newPosts;
      },
    },
  });

  posts.fetch = async () => {
    // Prevent unnecessary page reloads
    if (Object.keys(posts.data).length > 25) {
      return;
    }
    posts.data = await Posts.query();
  };

  posts.get = (postId) => {
    if (!posts.data[postId]) {
      posts.data[postId] = Posts.get({postId});
    }

    return posts.data[postId];
  };

  posts.getBy = (userId) => {
    const userPosts = Posts.getBy({userId});

    return userPosts;
  };

  posts.upvote = (postId) => {
    posts.data[postId].rank += 1;
  };

  posts.downvote = (postId) => {
    posts.data[postId].rank -= 1;
  };

  return posts;
}];
