module.exports = {
    "env": {
        "browser": true,
        "node": true,
        "es6": true
    },
    "extends": [
        "eslint:recommended",
        "eslint-config-canonical",
    ],
    "parserOptions": {
        "sourceType": "module",
        "ecmaVersion": 8,
    },
    "rules": {
        "filenames/match-regex": 0,
        "comma-dangle": ["error","always-multiline"],
        "id-match": 0,
        "import/no-unassigned-import": 1,
    },
    "plugins": [
        "angular",
    ]
};