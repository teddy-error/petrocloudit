/* eslint-disable */
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

const ENV = process.env.npm_lifecycle_event;
const isTest = ENV === 'test' || ENV === 'test-watch';
const isProd = ENV === 'build';

module.exports = (function makeWebpackConfig () {
  const config = {};

  config.entry = isTest ? {} :
    ['babel-polyfill', './src/app/app.js'];
  config.output = isTest ? {} : {
    path: __dirname + '/dist',
    publicPath: isProd ? '/' : 'http://localhost:8080/',
    filename: isProd ? '[name].[hash].js' : '[name].bundle.js',
    chunkFilename: isProd ? '[name].[hash].js' : '[name].bundle.js',
  };

  // Development source maps
  if (isTest) {
  config.devtool = 'eval-source-map';
  }

  // Initialize module
  config.module = {
    rules: [{
      test: /\.js$/,
      use: [
        'ng-annotate-loader',
        'babel-loader',
      ],
      exclude: /node_modules/,
    }, {
      test: /\.css$/,
      loader: isTest ? 'null-loader' : ExtractTextPlugin.extract({
        fallbackLoader: 'style-loader',
        loader: [
          {loader: 'css-loader',
            query: {sourceMap: true}},
          {loader: 'postcss-loader'},
        ],
      }),
    }, {
      test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
      loader: 'file-loader',
    }, {
      test: /\.html$/,
      loader: 'raw-loader',
    }],
  };

  if (isTest) {
    config.module.rules.push({
      enforce: 'pre',
      test: /\.js$/,
      exclude: [
        /node_modules/,
        /\.spec\.js$/,
      ],
      loader: 'istanbul-instrumenter-loader',
      query: {
        esModules: true,
      },
    });
  }

  config.plugins = [
    new webpack.LoaderOptionsPlugin({
      test: /\.scss$/i,
      options: {
        postcss: {
          plugins: [autoprefixer],
        },
      },
    }),
  ];

  if (!isTest) {
    config.plugins.push(
      new HtmlWebpackPlugin({
        template: './src/public/index.html',
        inject: 'body',
      }),
      new ExtractTextPlugin({filename: 'css/[name].css',
        disable: !isProd,
        allChunks: true})
    );
  }

  if (isProd) {
    config.plugins.push(
      // Only emit files when there are no errors
      new webpack.NoEmitOnErrorsPlugin(),

      // Copy assets from the public folder
      new CopyWebpackPlugin([{
        from: __dirname + '/src/public',
      }]),
      new UglifyJsPlugin({
        sourceMap: true,
        parallel: true,
      }),
    );
  }

  /**
   * Dev server configuration
   * Reference: http://webpack.github.io/docs/configuration.html#devserver
   * Reference: http://webpack.github.io/docs/webpack-dev-server.html
   */
  config.devServer = {
    contentBase: './src/public',
    stats: 'minimal',
  };

  return config;
})();
